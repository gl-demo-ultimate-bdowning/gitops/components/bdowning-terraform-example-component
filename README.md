# Published Catalog Usage


[![pipeline status](https://gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/badges/main/pipeline.svg)](https://gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/-/commits/main)


[![Latest Release](https://gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/-/badges/release.svg)](https://gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/-/releases)

## Terraform Validate

### To add to your CI File:

```yaml
  - component: gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/terraform-validate@<VERSION>
    inputs:
      stage: validate
```


| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`  | `validate` | The pipeline stage where to add the Terrafor validate job |


## Terraform Build

### To add to your CI File:

```yaml
  - component: gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/terraform-build@<VERSION>
    inputs:
      stage: build
```


| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`  | `build` | The pipeline stage where to add the Terrafor build job |


## Terraform Deploy

### To add to your CI File:

```yaml
  - component: gitlab.com/gl-demo-ultimate-bdowning/gitops/components/bdowning-terraform-example-component/terraform-deploy@<VERSION>
    inputs:
      stage: deploy
```


| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`  | `deploy` | The pipeline stage where to add the Terrafor Deploy job |
